package com.example.locationtracker;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.locationtracker.helper.LocationBroadcast;
import com.example.locationtracker.models.LiveTracking;
import com.example.locationtracker.utils.AppConstants;
import com.example.locationtracker.utils.DateUtils;
import com.example.locationtracker.utils.GPSUtils;
import com.google.gson.Gson;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private boolean isGPS = false;
    private String startTime,stopTime;
    SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RequestPermission();

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = DateUtils.getTime();
                startBroadCastReceiver();
            }
        });


        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                killBroadCastReceiver();
                stopTime = DateUtils.getTime();
            }
        });




    }


    private void StoreData(LiveTracking data){


        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(data);
        prefsEditor.putString("LOC_DATA", json);
        prefsEditor.commit();
    }


    private LiveTracking getData(){
        Gson gson = new Gson();
        String json = mPrefs.getString("LOC_DATA", "");
       return gson.fromJson(json, LiveTracking.class);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true; // flag maintain before get location
            }
        }
    }






    private void RequestPermission()
    {
        if(!isGPS){
            new GPSUtils(this).turnGPSOn(new GPSUtils.onGpsListener() {

                @Override
                public void gpsStatus(boolean isGPSEnable) {
                    // turn on GPS
                    isGPS = isGPSEnable;
                }
            });
        }

    }

    private void startBroadCastReceiver() {
        PackageManager pm = this.getPackageManager();
        ComponentName componentName = new ComponentName(this, LocationBroadcast.class);
        pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void killBroadCastReceiver() {
        PackageManager pm = this.getPackageManager();
        ComponentName componentName = new ComponentName(this, LocationBroadcast.class);
        pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

}