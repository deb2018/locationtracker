package com.example.locationtracker.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.locationtracker.models.LiveTracking;
import com.example.locationtracker.service.GPSService;

import org.joda.time.LocalTime;

public class LocationBroadcast extends BroadcastReceiver {

    Context context;
    //the method will be fired when the alarm is triggerred
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        GPSService service = new GPSService(context);
        LiveTracking data = new LiveTracking();

//        data.setDate(DateUtils.getTime());
//        data.setLat(service.getLatitude());
//        data.setLng(service.getLongitude());

        Toast.makeText(context,String.valueOf(service.getLatitude()),Toast.LENGTH_SHORT).show();



    }

}
