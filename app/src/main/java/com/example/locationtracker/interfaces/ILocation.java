package com.example.locationtracker.interfaces;

public interface ILocation {

    void locationResult(double lat,double lng,double accuracy);
}
