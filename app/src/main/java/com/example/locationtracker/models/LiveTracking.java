package com.example.locationtracker.models;

import java.util.ArrayList;

public class LiveTracking {

    private String trip_id;
    private String start_time;
    private String end_time;
    private ArrayList<Location> locations;


    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    public class Location{

        private double latitude;
        private double longitide;
        private double accuracy;
        private String timestamp;

    }

}


